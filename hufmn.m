fileID = fopen('korobeiniki.mp3','r');
[decimalAudio,M] = fread(fileID);
fclose(fileID);
binaryAudio = de2bi(decimalAudio);
nibbleAudio = ByteToNibble(binaryAudio,M);
N = 2*M
%%
sample = de2bi((0:15)');
for i = 1:16
    sample(i,5) = i;
    sample(i,6) = HowMany(nibbleAudio,sample(i,1:4),N)/N;
end
%%
uncodedProbs = sample(:,5:6);

%%
codes = huffmandict(uncodedProbs(:,1)',uncodedProbs(:,2)');
%%
codedAudio = ApplyCoded(nibbleAudio,N,codes);
%%
invCodedAudio = InversionEnc(nibbleAudio,4,N);
%%
sample(:,7) = 0:15;
samplee = sortrows(sample,-6);
%%
myCell = cell(16,3);
for i=1:16
    myCell{i,1} = samplee(i,7);
    myCell{i,2} = samplee(i,6);
end

ShannonFanoCodes = ShannonFanoCodeMaker(myCell,16);
ShannonFanoCodes2 = sortrows(ShannonFanoCodes,1);
ShannonFanoCoded = ApplyCoded(nibbleAudio,N,ShannonFanoCodes2(:,[1,3]));
%% ActivityCoef
firstActCoef = Missmatch(nibbleAudio)/(4*N);
invActCoef = Missmatch(invCodedAudio)/(4*N); %if we don't consider additinal bits
hufActCoef = Missmatch(reshape(codedAudio,4,1855527)')/(7422108);
shannActCoef = Missmatch(reshape(ShannonFanoCoded,4,1864084)')/(7456336);
%% INVERSE :|
invShan = InvShannon(ShannonFanoCoded);
% invHuf = InvHuf(codedAudio);
invHuf =  de2bi(huffmandeco(codedAudio,codes) - 1);
invInv = InvInv(invCodedAudio);
