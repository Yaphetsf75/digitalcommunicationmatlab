function [ y ] = InvShannon( x )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
m = 1;
n = 0;
Z = zeros(length(x),4);
while(m<=length(x))
    n = n+1;
    if(x(m)==0)
        if(x(m+1)==0)
            if(x(m+2)==0)
                Z(n,:) = [0 0 0 0];
                m=m+3;
                continue;
            else
                if(x(m+3)==0)
                    Z(n,:) = [0 1 0 0];
                    m=m+4;
                    continue;
                else
                    Z(n,:) = [0 0 0 1];
                    m=m+4;
                    continue;
                end
            end
        else
            if(x(m+2)==0)
                if(x(m+3)==0)
                    Z(n,:) = [0 0 1 0];
                    m=m+4;
                    continue;
                else
                    Z(n,:) = [1 0 0 0];
                    m=m+4;
                    continue;
                end
            else
                if(x(m+3)==0)
                    Z(n,:) = [1 0 0 1];
                    m=m+4;
                    continue;
                else
                    Z(n,:) = [1 1 0 0];
                    m=m+4;
                    continue;
                end
            end
        end
    else
        if(x(m+1)==0)
            if(x(m+2)==0)
                if(x(m+3)==0)
                    if(x(m+4)==0)
                        Z(n,:) = [0 1 1 0];
                        m=m+5;
                        continue;
                    else
                        Z(n,:) = [0 0 1 1];
                        m=m+5;
                        continue;
                    end
                else
                    Z(n,:) = [0 1 0 1];
                    m=m+4;
                    continue;
                end
            else
                if(x(m+3)==0)
                    Z(n,:) = [1 0 1 0];
                    m=m+4;
                    continue;
                else
                    Z(n,:) = [1 1 1 1];
                    m=m+4;
                    continue;
                end
            end
        else
            if(x(m+2)==0)
                if(x(m+3)==0)
                    Z(n,:) = [1 0 1 1];
                    m=m+4;
                    continue;
                else
                    Z(n,:) = [1 1 0 1];
                    m=m+4;
                    continue;
                end
            else
                if(x(m+3)==0)
                    Z(n,:) = [0 1 1 1];
                    m=m+4;
                    continue;
                else
                    Z(n,:) = [1 1 1 0];
                    m=m+4;
                    continue;
                end
            end
        end
    end
end
y = Z(1:(n),:);
end