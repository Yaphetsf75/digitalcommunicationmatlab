function [ y ] = Missmatch( x )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
N = size(x,1) - 1;
z = zeros(N,4);
for i=1:N
    z(i,:) = xor(x(i+1,1:4),x(i,1:4));
end
y = sum(sum(z));
end

