function [ myCell ] = ShannonFanoCodeMaker( myCell, N )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
if (N==1)
    return
end
if (N==2)
    if (myCell{1,2}>myCell{2,2})
       myCell{1,3} = [[myCell{1,3}],0];
       myCell{2,3} = [[myCell{2,3}],1];
    else
       myCell{1,3} = [[myCell{1,3}],1];
       myCell{2,3} = [[myCell{2,3}],0];
    end
    return
end
H = zeros(N,1);
x = [myCell{:,2}]';
for i=1:N
    H(i) = sum(x(1:i,1));
end
a2 = sum(H < 0.5*sum(x)) + 1;
for j=1:a2
    myCell{j,3} = [[myCell{j,3}],0];
    y = [myCell{:,2}]';
end
myCell(1:a2,:) = ShannonFanoCodeMaker(myCell(1:a2,:),a2);
for j=a2+1:N
    myCell{j,3} = [[myCell{j,3}],1];
    y = [myCell{:,2}]';
end
myCell(a2+1:N,:) = ShannonFanoCodeMaker(myCell(a2+1:N,:),N-a2);

end