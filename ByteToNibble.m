function [ y ] = ByteToNibble( x , N )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
z = zeros(2*N,4);
for i = 1:N
    z(2*i,:) = x(i,1:4);
    z(2*i-1,:) = x(i,5:8);
end

y = z;

end