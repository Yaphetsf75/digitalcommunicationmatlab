function [ y ] = InvInv( x )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
N = size(x,1);
y = zeros(N,4);
for i=1:N
    if(x(i,5)==0)
        y(i,:) = x(i,1:4);
    else
        y(i,:) = ~x(i,1:4);
    end
end
end

