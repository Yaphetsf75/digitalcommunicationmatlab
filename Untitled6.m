sampleI = de2bi((0:15)');
for i = 1:16
    sampleI(i,5) = i;
    sampleI(i,6) = HowMany(reshape(codedAudio,4,1855527)',sampleI(i,1:4),1855527)/1855527;
end
%%
uncodedProbsI = sampleI(:,5:6);

%%
codesI = huffmandict(uncodedProbsI(:,1)',uncodedProbsI(:,2)');
%%
codedAudioI = ApplyCoded(reshape(codedAudio,4,1855527)',1855527,codesI);
%%
hufActCoef = Missmatch(reshape(codedAudioI,4,1840189)')/(7360756);
